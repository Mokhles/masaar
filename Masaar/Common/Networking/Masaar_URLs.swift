//
//  Masaar_URLs.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import Foundation

extension MassarAPIs
{
    struct URLs
    {
        static let baseURL              = "https://dhcr.gov.ae/MobileWebAPI/api/"
        static let departmentServices   = baseURL + "Common/ServiceCatalogue/GetDepartmentServices"
    }
}

