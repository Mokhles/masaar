//
//  Massar_APIs.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//MARK: - CompletionHaander

typealias CompletionHandler   = (_ status : Bool,_ response: DepartmentServicesModel?) -> ()


class MassarAPIs
{
    static let sharedInstance = MassarAPIs()
    
    func getDepartmentServices(pageSize     : Int,
                               pageIndex    : Int,
                               departmentID : Int,
                               completion   : @escaping CompletionHandler)
    {
        
        let parameters: [String: Any] = [
            "PageSize"         : pageSize,
            "PageIndex"        : pageIndex,
            "DepartmentID"     : departmentID
        ]
        
        Alamofire.request(URLs.departmentServices,
                          method    : .post,
                          parameters: parameters,
                          encoding  : JSONEncoding.default).responseJSON { (response) -> Void in
                            
                            if response.result.isSuccess
                            {
                                if let value = response.result.value
                                {
                                    completion(true, self.parseResponseValue(value: value))
                                }
                            }
                            else
                            {
                                let error = response.result.error
                                print(error?.localizedDescription as Any)
                                completion(false, nil)
                            }
        }
    }
    
    //MARK: - Parsing
    
    func parseResponseValue(value: Any) -> DepartmentServicesModel
    {
        let json     = JSON(value)
        let data    = json["Data"]
        
        var departmentServices : DepartmentServicesModel
        
        var dataResponse = [DepartmentDataModel]()
        
        data.array?.forEach({ (data) in
            
            let dataID                = data["ID"].intValue
            let subTitle              = data["Brief"].stringValue
            let parentTitle           = data["Title"].stringValue
            let imageUrl              = data["ImageSrc"].stringValue
            let description           = data["Description"].stringValue
            let prerequisites         = data["Prerequisites"].stringValue
            let requiredDocuments     = data["RequiredDocuments"].stringValue
            let fees                  = data["Fees"].stringValue
            let timeFrame             = data["TimeFrame"].stringValue
            let serviceChannels       = data["ServiceChannels"].stringValue
            let policiesAndProcedures = data["PoliciesAndProcedures"].stringValue
            
            let data = DepartmentDataModel.init(id: dataID, parentTitle: parentTitle, subTitle: subTitle, imageUrl: imageUrl, description: description, prerequisites: prerequisites, requiredDocuments: requiredDocuments, fees: fees, timeFrame: timeFrame, serviceChannels: serviceChannels, policiesAndProcedures: policiesAndProcedures)
            
            dataResponse.append(data)
            
        })
        
        let isSucceed      = json["IsSucceed"].boolValue
        let messagesCode   = json["MessagesCode"].int
        
        departmentServices = DepartmentServicesModel.init(isSucceed: isSucceed, messageCode: messagesCode, dataModle: dataResponse)
        
        return departmentServices
    }
}
