//
//  HelperFunctions.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import UIKit

class HelperFunctions
{
    static func showErrorAlert(view: UIViewController?, title: String, msg: String) -> Void
    {
        guard let view = view else { return }
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        
        view.present(alert, animated: true, completion: nil)
    }
}



