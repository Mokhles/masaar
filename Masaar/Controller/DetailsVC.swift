//
//  DetailsVC.swift
//  Masaar
//
//  Created by MarkMokhles on 8/6/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import UIKit
import Kingfisher

class DetailsVC: UIViewController
{
    
    //MARK: - iVars
    
    var departmentDetailsData = DepartmentDataModel()
    
    //MARK: - iBoutlets
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var prerequistiesLbl: UILabel!
    @IBOutlet weak var requiredDocLbl: UILabel!
    @IBOutlet weak var feesLbl: UILabel!
    @IBOutlet weak var timeFrameLbl: UILabel!
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var policiesLbl: UILabel!
    @IBOutlet weak var assistLbl: UILabel!
    @IBOutlet weak var detailsImageView: UIImageView!
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Set Data to view
        setViewData()
        
        //Set NavigationBar Image
        let yourBackImage = UIImage(named: "left-arrow")
        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
        
        //Set NavigationBar Title
        self.title = departmentDetailsData.parentTitle
    }
    
    //MARK: - Helper Functions
    
    func setViewData()
    {
        descriptionLabel.text = departmentDetailsData.description?.htmlToString
        prerequistiesLbl.text = departmentDetailsData.prerequisites?.htmlToString
        requiredDocLbl.text = departmentDetailsData.requiredDocuments?.htmlToString
        feesLbl.text = departmentDetailsData.fees?.htmlToString
        timeFrameLbl.text = departmentDetailsData.timeFrame?.htmlToString
        serviceLbl.text = departmentDetailsData.serviceChannels?.htmlToString
        policiesLbl.text = departmentDetailsData.policiesAndProcedures?.htmlToString
        
        if departmentDetailsData.imageUrl != nil
        {
            detailsImageView.kf.indicatorType = .activity
            
            let url = URL(string: departmentDetailsData.imageUrl!)!
            
            detailsImageView.kf.setImage(with: url)
        }
    }
}
