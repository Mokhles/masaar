//
//  HomeVC.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import UIKit
import MBProgressHUD

class HomeVC: UIViewController
{
    
    //MARK: - iVars
    
    var pageNum: Int = 10
    var pageIndex: Int = 1
    var departmentDataArr = [DepartmentDataModel]()
    
    //MARK: - iBoutlets
    
    @IBOutlet weak var homeTableView: UITableView!
    
    //MARK: - View Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //TableView Delegate
        homeTableView.delegate = self
        homeTableView.dataSource = self
        homeTableView.tableFooterView = UIView()
        
        //Register TableViewCell
        homeTableView.registerCellNib(cellClass: HomeTableViewCell.self)
        
        //Get Data
        setFacilitiesScreen (pageIndex: pageIndex)
    }
    
    //MARK: - Helper Functions
    
    func setFacilitiesScreen (pageIndex: Int)
    {
        // For ProgressIndicator
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "loading"
        
        MassarAPIs.sharedInstance.getDepartmentServices(pageSize: 10, pageIndex: pageIndex, departmentID: 2) { (success , response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if response?.messageCode == 0
            {
                if (response?.isSucceed)!
                {
                    self.departmentDataArr.append(contentsOf: response!.dataModle)
                    self.homeTableView.reloadData()
                }
                else
                {
                    HelperFunctions.showErrorAlert(view: self, title: "Attention!", msg: "Services will be available soon..")
                }
            }
            else
            {
                HelperFunctions.showErrorAlert(view: self, title: "Attention!", msg: "Something Wrong is Happend")
            }
        }
    }
    
    //Pagination
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if ((homeTableView.contentOffset.y + homeTableView.frame.size.height) >= homeTableView.contentSize.height)
        {
            if departmentDataArr.count == pageNum
            {
                pageIndex += 1
                pageNum += 10
                setFacilitiesScreen (pageIndex: pageIndex)
            }
            else
            {
                HelperFunctions.showErrorAlert(view: self, title: "Info", msg: "No more data")
            }
        }
    }
}

//MARK: - UITableView Data Source

extension HomeVC: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return departmentDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = homeTableView.dequeue() as HomeTableViewCell
        
        cell.setModel(model: departmentDataArr[indexPath.row])
        
        return cell
    }
}

//MARK: - UITableView Delegate

extension HomeVC: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let detailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsVC") as? DetailsVC
        {
            if let navigator = navigationController
            {
                detailsVC.departmentDetailsData = departmentDataArr[indexPath.row]
                navigator.pushViewController(detailsVC, animated: true)
            }
        }
    }
}

