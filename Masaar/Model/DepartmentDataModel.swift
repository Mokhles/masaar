//
//  DataModel.swift
//  Masaar
//
//  Created by VictoryLink on 8/7/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import Foundation

struct DepartmentDataModel
{
    var id                    : Int?
    var parentTitle           : String?
    var subTitle              : String?
    var imageUrl              : String?
    var description           : String?
    var prerequisites         : String?
    var requiredDocuments     : String?
    var fees                  : String?
    var timeFrame             : String?
    var serviceChannels       : String?
    var policiesAndProcedures : String?
}
