//
//  DepartmentServicesModel.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import Foundation

struct DepartmentServicesModel
{
    var isSucceed             : Bool?
    var messageCode           : Int?
    var dataModle             : [DepartmentDataModel]
}

