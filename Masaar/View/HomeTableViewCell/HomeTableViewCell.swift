//
//  HomeTableViewCell.swift
//  Masaar
//
//  Created by MarkMokhles on 8/5/19.
//  Copyright © 2019 MarkMokhles. All rights reserved.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell
{
    //MARK: - iBoutlets
    
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subHeaderLabel: UILabel!
    
    //MARK: - Helper Functions
    
    func setModel(model: DepartmentDataModel)
    {
        headerLabel.text = model.parentTitle
        subHeaderLabel.text = model.subTitle
        
        if model.imageUrl != nil
        {
            homeImageView.kf.indicatorType = .activity
            
            let url = URL(string: model.imageUrl!)!
            
            homeImageView.kf.setImage(with: url)
        }
    }
}



