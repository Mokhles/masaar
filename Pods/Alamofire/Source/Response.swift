//
//  Response.swift
//
//  Copyright (c) 2014 Alamofire Software Foundation (http://alamofire.org/)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation

/// Used to store all data associated with an non-serialized response of a data or upload request.
public struct DefaultDataResponse {
    /// The URL request sent to the server.
    public let request: URLRequest?

    /// The server's response to the URL request.
    public let response: HTTPURLResponse?

    /// The data returned by the server.
    public let data: Data?

    /// The error encountered while executing or validating the request.
    public let error: Error?

    /// The timeline of the complete lifecycle of the request.
    public let timeline: Timeline

    var _metrics: AnyObject?

    /// Creates a `DefaultDataResponse` instance from the specified parameters.
    ///
    /// - Parameters:
    ///   - request:  The URL request sent to the server.
    ///   - response: The server's response to the URL request.
    ///   - data:     The data returned by the server.
    ///   - error:    The error encountered while executing or validating the request.
    ///   - timeline: The timeline of the complete lifecycle of the request. `Timeline()` by default.
    ///   - metrics:  The task metrics containing the request / response statistics. `nil` by default.
    public init(
        request: URLRequest?,
        response: HTTPURLResponse?,
        data: Data?,
        error: Error?,
        timeline: Timeline = Timeline(),
        metrics: AnyObject? = nil)
    {
        self.request = request
        self.response = response
        self.data = data
        self.error = error
        self.timeline = timeline
    }
}

// MARK: -

/// Used to store all data associated with a serialized response of a data or upload request.
public struct DataResponse<Value> {
    /// The URL request sent to the server.
    public let request: URLRequest?

    /// The server's response to the URL request.
    public let response: HTTPURLResponse?

    /// The data returned by the server.
    public let data: Data?

    /// The result of response serialization.
    public let result: Result<Value>

    /// The timeline of the complete lifecycle of the request.
    public let timeline: Timeline

    /// Returns the associated value of the result if it is a success, `nil` otherwise.
    public var value: Value? { return result.value }

    /// Returns the associated error value if the result if it is a failure, `nil` otherwise.
    public var error: Error? { return result.error }

    var _metrics: AnyObject?

    /// Creates a `DataResponse` instance with the specified parameters derived from response serialization.
    ///
    /// - parameter request:  The URL request sent to the server.
    /// - parameter response: The server's response to the URL request.
    /// - parameter data:     The data returned by the server.
    /// - parameter result:   The result of response serialization.
    /// - parameter timeline: The timeline of the complete lifecycle of the `Request`. Defaults to `Timeline()`.
    ///
    /// - returns: The new `DataResponse` instance.
    public init(
        request: URLRequest?,
        response: HTTPURLResponse?,
        data: Data?,
        result: Result<Value>,
        timeline: Timeline = Timeline())
    {
        self.request = request
        self.response = response
        self.data = data
        self.result = result
        self.timeline = timeline
    }
}

// MARK: -

extension DataResponse: CustomStringConvertible, CustomDebugStringConvertible {
    /// The textual representation used when written to an output stream, which includes whether the result was a
    /// success or failure.
    public var description: String {
        return result.debugDescription
    }

    /// The debug textual representation used when written to an output stream, which includes the URL request, the URL
    /// response, the server data, the response serialization result and the timeline.
    public var debugDescription: String {
        var output: [String] = []

        output.append(request != nil ? "[Request]: \(request!.httpMethod ?? "GET") \(request!)" : "[Request]: nil")
        output.append(response != nil ? "[Response]: \(response!)" : "[Response]: nil")
        output.append("[Data]: \(data?.count ?? 0) bytes")
        output.append("[Result]: \(result.debugDescription)")
        output.append("[Timeline]: \(timeline.debugDescription)")

        return output.joined(separator: "\n")
    }
}

// MARK: -

extension DataResponse {
    /// Evaluates the specified closure when the result of this `DataResponse` is a success, passing the unwrapped
    /// result value as a parameter.
    ///
    /// Use the `map` method with a closure that does not throw. For example:
    ///
    ///     let possibleData: DataResponse<Data> = ...
    ///     let possibleInt = possibleData.map { $0.count }
    ///
    /// - parameter transform: A closure that takes the success value of the instance's result.
    ///
    /// - returns: A `DataResponse` whose result wraps the value returned by the given closure. If this instance's
    ///            result is a failure, returns a response wrapping the same failure.
    public func map<T>(_ transform: (Value) -> T) -> DataResponse<T> {
        var response = DataResponse<T>(
            request: request,
            response: self.response,
            data: data,
            result: result.map(transform),
            timeline: timeline
        )

        response._metrics = _metrics

        return response
    }

    /// Evaluates the given closure when the result of this `DataResponse` is a success, passing the unwrapped result
    /// value as a parameter.
    ///
    /// Use the `flatMap` method with a closure that may throw an error. For example:
    ///
    ///     let possibleData: DataResponse<Data> = ...
    ///     let possibleObject = possibleData.flatMap {
    ///         try JSONSerialization.jsonObject(with: $0)
    ///     }
    ///
    /// - parameter transform: A closure that takes the success value of the instance's result.
    ///
    /// - returns: A success or failure `DataResponse` depending on the result of the given closure. If this instance's
    ///            result is a failure, returns the same failure.
    public func flatMap<T>(_ transform: (Value) throws -> T) -> DataResponse<T> {
        var response = DataResponse<T>(
            request: request,
            response: self.response,
            data: data,
            result: result.flatMap(transform),
            timeline: timeline
        )

        response._metrics = _metrics

        return response
    }

    /// Evaluates the specified closure when the `DataResponse` is a failure, passing the unwrapped error as a parameter.
    ///
    /// Use the `mapError` function with a closure that does not throw. For example:
    ///
    ///     let possibleData: DataResponse<Data> = ...
    ///     let withMyError = possibleData.mapError { MyError.error($0) }
    ///
    /// - Parameter transform: A closure that takes the error of the instance.
    /// - Returns: A `DataResponse` instance containing the result of the transform.
    public func mapError<E: Error>(_ transform: (Error) -> E) -> DataResponse {
        var response = DataResponse(
            request: request,
            response: self.response,
            data: data,
            result: result.mapError(transform),
            timeline: timeline
        )

        response._metrics = _metrics

        return response
    }

    /// Evaluates the specified closure when the `DataResponse` is a failure, passing the unwrapped error as a parameter.
    ///
    /// Use the `flatMapError` function with a closure that may throw an error. For example:
    ///
    ///     let possibleData: DataResponse<Data> = ...
    ///     let possibleObject = possibleData.flatMapError {
    ///         try someFailableFunction(taking: $0)
    ///     }
    ///
    /// - Parameter transform: A throwing closure that takes the error of the instance.
    ///
    /// - Returns: A `DataResponse` instance containing the result of the transform.
    public func flatMapError<E: Error>(_ transform: (Error) throws -> E) -> DataResponse {
        var response = DataResponse(
            request: request,
            response: self.response,
            data: data,
            result: result.flatMapError(transform),
            timeline: timeline
        )

        response._metrics = _metrics

        return response
    }
}

// MARK: -

/// Used to store all data associated with an non-serialized response of a download request.
public struct DefaultDownloadResponse {
    /// The URL request sent to the server.
    public let request: URLRequest?

    /// The server's response to the URL request.
    public let response: HTTPURLResponse?

    /// The temporary destination URL of the data returned from the server.
    public let temporaryURL: URL?

    /// The final destination URL of the data returned from the server if it was moved.
    public let destinationURL: URL?

    /// The resume data generated if the request was cancelled.
    public let resumeData: Data?

    /// The error encountered while executing or validating the request.
    public let error: Error?

    /// The timeline of the complete lifecycle of the request.
    public let timeline: Timeline

    var _metrics: AnyObject?

    /// Creates a `DefaultDownloadResponse` instance from the specified parameters.
    ///
    /// - Parameters:
    ///   - request:        The URL request sent to the server.
    ///   - response:       The server's response to the URL request.
    ///   - temporaryURL:   The temporary destination URL of the data returned from the server.
    ///   - destinationURL: The final destination URL of the data returned from the server if it was moved.
    ///   - resumeData:     The resume data generated if the request was cancelled.
    ///   - error:          The error encountered while executing or validating the request.
    ///   - timeline:       The timeline of the complete lifecycle of the request. `Timeline()` by default.
    ///   - metrics:        The task metrics containing the request / response statistics. `nil` by default.
    public init(
        request: URLRequest?,
        response: HTTPURLResponse?,
        temporaryURL: URL?,
        destinationURL: URL?,
        resumeData: Data?,
        error: Error?,
        timeline: Timeline = Timeline(),
        metrics: AnyObject? = nil)
    {
        self.request = request
        self.response = response
        self.temporaryURL = temporaryURL
        self.destinationURL = destinationURL
        self.resumeData = resumeData
        self.error = error
        self.timeline = timeline
    }
}

// MARK: -

/// Used to store all data associated with a serialized response of a download request.
public struct DownloadResponse<Value> {
    /// The URL request sent to the server.
    public let request: URLRequest?

    /// The server's response to the URL request.
    public let response: HTTPURLResponse?

    /// The temporary destination URL of the data returned from the server.
    public let temporaryURL: URL?

    /// The final destination URL of the data returned from the server if it was moved.
    public let destinationURL: URL?

    /// The resume data generated if the request was cancelled.
    public let resumeData: Data?

    /// The result of response serialization.
    public let result: Result<Value>

    /// The timeline of the complete lifecycle of the request.
    public let timeline: Timeline

    /// Returns the associated value of the result if it is a success, `nil` otherwise.
    public var value: Value? { return result.value }

    /// Returns the associated error value if the result if it is a failure, `nil` otherwise.
    public var error: Error? { return result.error }

    var _metrics: AnyObject?

    /// Creates a `DownloadResponse` instance with the specified parameters derived from response serialization.
    ///
    /// - parameter request:        The URL request sent to the server.
    /// - parameter response:       The server's response to the URL request.
    /// - parameter temporaryURL:   The temporary destination URL of the data returned from the server.
    /// - parameter destinationURL: The final destination URL of the data returned from the server if it was moved.
    /// - parameter resumeData:     The resume data generated if the request was cancelled.
    /// - parameter result:         The result of response serialization.
    /// - parameter timeline:       The timeline of the complete lifecycle of the `Request`. Defaults to `Timeline()`.
    ///
    /// - returns: The new `DownloadResponse` instance.
    public init(
        request: URLRequest?,
        response: HTTPURLResponse?,
        temporaryURL: URL?,
        destinationURL: URL?,
        resumeData: Data?,
        result: Result<Value>,
        timeline: Timeline = Timeline())
    {
        self.request = request
        self.response = response
        self.temporaryURL = temporaryURL
        self.destinationURL = destinationURL
        self.resumeData = resumeData
        self.result = result
        self.timeline = timeline
    }
}

// MARK: -

extension DownloadResponse: CustomStringConvertible, CustomDebugStringConvertible {
    /// The textual representation used when written to an output stream, which includes whether the result was a
    /// success or failure.
    public var description: String {
        return result.debugDescription
    }

    /// The debug textual representation used when written to an output stream, which includes the URL request, the URL
    /// response, the temporary and destination URLs, the resume data, the response serialization result and the
    /// timeline.
    public var debugDescription: String {
        var output: [String] = []

        output.append(request != nil ? "[Request]: \(request!.httpMethod ?? "GET") \(request!)" : "[Request]: nil")
        output.append(response != nil ? "[Response]: \(response!)" : "[Response]: nil")
        output.append("[TemporaryURL]: \(temporaryURL?.path ?? "nil")")
        output.append("[DestinationURL]: \(destinationURL?.path ?? "nil")")
        output.append("[ResumeData]: \(resumeData?.count ?? 0) bytes")
        output.append("[Result]: \(result.debugDescription)")
        output.append("[Timeline]: \(timeline.debugDescription)")

        return output.joined(separator: "\n")
    }
}

// MARK: -

extension DownloadResponse {
    /// Evaluates the given closure when the result of this `DownloadResponse` is a success, passing the unwrapped
    /// result value as a parameter.
    ///
    /// Use the `map` method with a closure that does not throw. For example:
    ///
    ///     let possibleData: DownloadResponse<Data> = ...
    ///     let possibleInt = possibleData.map { $0.count }
    ///
    /// - parameter transform: A closure that takes the success value of the instance's result.
    ///
    /// - returns: A `DownloadResponse` whose result wraps the value returned by the given closure. If this instance's
    ///            result is a failure, returns a response wrapping the same failure.   0rwBl�c vunc map<T>(_ transooRo: (Valuei!-� D) )> TownlgadRms`oNse<�>"Z
  d   ( var re�po~se = DO7nl/adV�rponse<T>( �!         zeqe�qt: sequest,  �`  ` !   respgnse: sulf.bdsponce,
    � $   !temporarYURL: tmMporaRyURH,
!    (      deqtiN�tionQRL: ddstina|�onURL,
0          rec�meDatc: RusumeData,
  ` �       re�ultz zesulp*m`p�traoSfozm)
 `          timeline:0timeline
!"      	

        rds�olse.[mmtrias = _metr)cs
� 0     reptro�res0o.sE
 @ �}

�   /// EvAlwatew tH� given clos�ve whef $h� peselt of vhis,pDownloa$Rewponseb as a success, q�sshno tje wnwrappef
   2//(r�su�p rIlum a� ` par mateP*
    /?/
$   /+/ Use the$`fmatMap`�iet`od Wit�aa cl�Syve �lat ma{0|hrow an $rroR. For"eximpne:
"  `//o
  ! /o/(    leu possible@ava| DownnoadVes�oNs%<Dada>`= �.>�   //-�    �e| �ossibleOb�e#t =!pms3�bleData,flqTMap${  ` //- (   (   pzy ZSONSe�ializatmon.hqonobjecthwith�"$0)
    //0    }
 2  -+/
  � /'/ - para,g4e�Transform: q clsur- that paog{ the suskess value jf t�e inspilce#3(�e3ulp.
    /'/�  ��///"/ rmtqrns� I`succgss nr0failure `Downlnadesponze` depending on pje rdwult of 0he�gaven c|osure. If thiS
 � �/?/ instan;e's zeSuht is a!vailure, ret5vjs 4he sam% &eihube/ `  public funk flavMap<T>(_!transfr} (V�lue) thrmw�(-? T�"-6 DowllnadResponse<t> 
!      v�R 2Espolse = Dow�loadRespmnse8T>(
    "0      repuast: vaqu%s|,
      "h  ` respn�se: qelf.responSe,       ! $0 tem`Or!ryWRD:(tempo�ar}URL<
     �  `  "de3�in�tkfnUL: besvinationURL,
    (" �    resumeData( vwumeData,    0!`0    rusult:$result.fla�Mip�tsan�form),
        0   timehi~e: f�lel)ne
    �   +
 $      reCpo.su.^metrigs = metriaw
        r�turn res�onre
 " 0y
J! , /// Evaluatms0txe(specified a,osu2e wHen the `D�wnl�ctRe�ronse` is8qpfai.ura, passing �le unwrappEd error ec a pcrdmeter.
    /?/
  # ///0Ucu The `mApT2rkrp ftnction ivz ! clo�ure1that does0nmp uhrw�(F/� Exampld:
    //�    ///  ( !l%t po3skblgD!vA> downhnadRespofse<@ara> = �..    /+/     ,et wi|hMYError = possIbledc}a.mapErzo2 y MyMrror.ewpor($0) }
( " '//
    /./8- Param%tes tr!.sform: A!cloSure tla�$tajes the error ?fdth% instan#e.�  $/// / R%tUrns
"E `Dmwndo�$Z%spon�g� )nstaocd ckntainioghtpe"reswlt of the tr�n�borm.
 0  pubhIs vunc mapdvrmr<E:!Errnr:(_ trin�form* (Error) >�E� -< @�wnm?adRdspo,se s
        ~ar zEsponse`= DngnloaD�dsrOnse(
      $  0` req}est: reqee7t, !  0       �erponsu: 3elf,nuc�onse,
    "�  l   tgm0�raryURX:#tEmporas}UL,
       "`   ddsTijationURL:(destine�ionURL�
  �     "  "reSwieDaUa: rds5}eData�
   (   " 0  resu,Tz resu|t.MapErbor(transform)�J(           timelIne2 timeline
   (   0)J
  !     responsu*_metriCs`"Wmqtrigs

        rmtu�� zesponse
 "$ }    '/>0Ev`luape� the�qpecifiee clos5re w(en(txe `Do�f,m�tRespo.s�`(is a oaile2e("passi�g$5`e`unwratped eRror�a3"a para}e�er/
    �//
p   //��U3u qhe dflatM!pErpor` functaoN$with ` glnSure |l!t0lay th�ow an -sror.)doz ex1mpne:J(   ///
  �!/// �  aleu`PorsibheData: DovjloAdSEs�onsd�D�ta< = *.�
    //�     lgt`possibleObjecd = possk��eData.fha��e`Erros {
    /+/       0 try SomeFalla�ldFunction(tek)ng: $�( �  ///  "  }
    ///!  -//�-`PErAmgter traNsfor}: A thrwi�g"chos5ru Pha4 ta+g� the esror /f t`e in3pance.J(   /�/
 �  /// % Retw0fs: A``Do7nhoe$��cponsE` instance(cntaioing$5h�`res�|t�of tle traNsfmbm
  b 0u`lic f5jc �,atMaxEz~op<E:0Error>*_!traJsform: (err�r) pH�ows�-> E) -> unloaFPespohre {
     $!$var rewtonse - Dounloades`�NsE(
   �    `   reqUest: r�quEct,
 (        + vespojse: self.re�polsd,
 "        ! temporaryURL:)ta-p�va2yRL�  �  00  �0 dmstinationERL:$DdstlnationURL,
    "  ��"  zesumeData:"resuiuDpta$
     0  $ ( recult:�re{ul�.FlatIapE2zo�(4riosform),
   "     !( t�meli�e: timelinu
 `  �   )
        resp�n�d._metrics = _matrics

 �`" (  return"respo.se
    }
}/ EMR
: -t�dkaot Resp/~se ;��   +// Tje tasK�ie4ric3c�ntai�ing the requg3t o rDsponse KTatipmcs,
 !  var _metricr: nyO`kuct? { Gdt set }
 `  muta4ing fenc add(W mgtr�gv2 AnyObject?)
}

ey�w�siOn�Rewponse {
 af&muuaping funG �dd(_ letric3: AnyOcject�) {
   `   $#iv`!o�hsatci(
$ ` `" `!" `guarf 3avaklableJiOS`10.1. macOS 10.12, tvOS 10.0, *) elsE!{hret5rf$u
`% $ (    " Guar$(leT )e4rac; = �a|ricw!as? TR~Ses{iOoT`siMetrics elre ~02%turn =
        �   _�%trics =`mutrkcs
!     `(�eneif	H  !~
]J//(MAvK: -
�P�6ailabme(iOS 10.0, macOR`�0.1, tvOS 10.0 *)
ex4ens�o�$D�daw�TDataResponqe: Resqonse�{
#if !o�(wau�hOS	
  0`/// Th� tasi mevrmbs c�ftaining 4he request$.$res`�nse stapkctics.
    pub|I" rar mudriks: QRLRusshmntiqkMetsics7`{�retubn [L�trhc as? URLSeqshontAsjMe|ricS ]
#e~`in

`!vAiiafhd(iOS 30.0=(iacoS 11>12, pvOQ 5 .1, #9
lxtens)on DcdaRm�poNs%z RdspNnsa {
#�f"!or(watciOQ)
�� "?//0�hE task metricw$cklt%in	ng txu rapuesd /�Sesxonse {t!tisti#{.
�   p�`lic(v�r metrigs: UD[es�iolTaryMetrics? z(jeuw�n _metzicS$er?0URse3sionVAwkMdDrigs1}#endmf
}J@a7aimab�e)iOS 40&0, macOS �0.12, TvOS 10.0, *)
exTensi* DefaultDowihoa$Reqponse: Responsg {
#I� !oshwauch'S)
! �$�//`��e(tasK metrics cgntailinw pjd rdqu�su / res�onre 3tqtistics>
 0  peblic �!�"mdtrics� URLSuscmonTa3kmetrics� { retern$_mEprics as? URLSeSsionuaskMevb)cs!}
#g�dif
}
@ava(labLE(iOS 5�.0,�iicOS 12.12, tvOW 10,l *)
Exteosi/, Do7lmmadResponqe8 Rdspo~sm y�if !os(wat#�OS)
(   +//$�he Ta{k meprhcs con|aining(the reu}e3d`/rr%spose {�a�iztic�
  " `ub�i#`vaz mgtrics:`URDSessionTaskMetjics?${ re�urj0_metrics as? URLSeSskonTascMetRics U#endyn
}
